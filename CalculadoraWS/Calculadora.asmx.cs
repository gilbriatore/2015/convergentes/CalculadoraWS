﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace CalculadoraWS
{
    [WebService(Namespace = "ws.up.edu.br")]
    public class Calculadora : System.Web.Services.WebService
    {
        [WebMethod]
        public int Somar(int a, int b)
        {
            return a + b;
        }
        [WebMethod]
        public int Subtrair(int a, int b)
        {
            return a - b;
        }
        [WebMethod]
        public int Multiplicar(int a, int b)
        {
            return a * b;
        }
        [WebMethod]
        public int Dividir(int a, int b)
        {
            return a / b;
        }
    }
}
