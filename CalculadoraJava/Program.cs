﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CalculadoraJava.CalculadoraService;

namespace CalculadoraJava
{
    class Program
    {
        static void Main(string[] args)
        {
            Calculadora c = new CalculadoraClient();
            int valor = c.somar(10, 5);
            Console.WriteLine("Adição: " + valor);
        }
    }
}
