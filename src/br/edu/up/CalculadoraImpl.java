package br.edu.up;

import javax.jws.WebService;

@WebService(endpointInterface = "br.edu.up.Calculadora")
public class CalculadoraImpl implements Calculadora {

	public int somar(int a, int b) {
		return a + b;
	}

	public int subtrair(int a, int b) {
		return a - b;
	}

	public int multiplicar(int a, int b) {
		return a * b;
	}

	public int dividir(int a, int b) {
		return a / b;
	}
}