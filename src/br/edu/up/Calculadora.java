package br.edu.up;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

@WebService
@SOAPBinding(style = Style.RPC)
public interface Calculadora {
	
	@WebMethod
	public int somar(int a, int b);
	
	@WebMethod
	public int subtrair(int a, int b);
	
	@WebMethod
	public int multiplicar(int a, int b);
	
	@WebMethod
	public int dividir(int a, int b);

}
